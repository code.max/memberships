<?php
defined( 'ABSPATH' ) || exit;
/*
 * Install Codemax > Memberships theme.
 * This is the child theme for Astra theme.
 */
add_action( 'wp_enqueue_scripts', 'cm_memberships_enqueue_styles' );
function cm_memberships_enqueue_styles() 
{
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', [ 'parent-style' ] );
}

/* Enable feature excerpt in pages.*/
function add_excerpt_in_page()
{
    add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'add_excerpt_in_page' );

/* Agrega el soporte de miniaturas personalizadas */
add_theme_support( 'post-thumbnails' );

require_once __DIR__. DIRECTORY_SEPARATOR . 'vendor/autoload.php';

/* Extension que muestra un grilla de publicaciones filtradas por nivel de suscripción  */
codemax\ui\shortcodes\CMPostsBySubscription::install();
