<?php
namespace codemax\ui\shortcodes;

abstract class CMPostsBySubscription
{
    
    /**
     * El shortcode muestra un grilla de publicaciones filtradas por nivel de suscripción. 
     */
    static public function install(): void
    {
        add_shortcode( 'cm_posts_by_subscription', [ CMPostsBySubscription::class, 'code' ] );
    }
    
    /**
     * 
     * @param array $atts
     * @return string
     */
    static public function code(): string
    {
        $code = '';
        if ( is_user_logged_in() )
        {
            $user_id = get_current_user_id();
            if ( function_exists( 'pms_is_member' ) )
            {
                if ( pms_is_member( $user_id, [ 46 ] ) )
                { // Filtro de miembros con la suscripción nivel Platino
                    $code = do_shortcode( '[the-post-grid id="189"]' );
                }
                elseif ( pms_is_member( $user_id, [ 48 ] ) )
                { // Filtro de miembros con la suscripción nivel Oro
                    $code = do_shortcode( '[the-post-grid id="192"]' );
                }
                elseif ( pms_is_member( $user_id, [ 49 ] ) )
                { // Filtro de miembros con la suscripción nivel Plata
                    $code = do_shortcode( '[the-post-grid id="196"]' );
                }
                else
                { // Filtro de miembros con la suscripción nivel Gratis
                    $code = do_shortcode( '[the-post-grid id="199"]' );
                }
            }
        }
        return $code;
    }
}

